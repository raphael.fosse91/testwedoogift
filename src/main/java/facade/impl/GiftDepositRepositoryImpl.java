package facade.impl;

import facade.GiftDepositRepository;
import model.Company;
import model.Employee;
import model.GiftDeposit;
import java.util.Calendar;
import java.util.Date;

public class GiftDepositRepositoryImpl implements GiftDepositRepository {

    /**
     * Methode permettant de crediter le compte cadeau
     * @param amount
     * @param company
     * @return giftDeposit
     */
    @Override
    public GiftDeposit sendGiftDeposit(int amount, Company company) {
        // Instanciation de l'objet
        GiftDeposit giftDeposit = new GiftDeposit();

        // Ajout du montant
        giftDeposit.setAmount(amount);

        // Ajout de la date d'expiration
        giftDeposit.setDateExpiration(calculDateExpiration());

        // Ajout de l'entreprise
        giftDeposit.setCompany(company);

        return giftDeposit;
    }

    /**
     *
     * @param employee
     * @return La somme sur la balance gift deposit
     */
    public int countBalance(Employee employee) {
        int balance = 0;
        if (employee.getGiftDepositBalance() != null) {
            for (int i = 0; i < employee.getGiftDepositBalance().size(); i++) {
                if (checkDate(employee.getGiftDepositBalance().get(i).getDateExpiration())) {
                    employee.getGiftDepositBalance().remove(i);
                    break;
                }
                balance += employee.getGiftDepositBalance().get(i).getAmount();
            }
        }
        return balance;
    }


    /**
     * Methode de vérification de la date d'expiration
     * @param date
     * @return true si la date a expiré
     */
    public boolean checkDate(Date date) {
        return date.before(new Date());
    }


    public Date calculDateExpiration() {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.YEAR, 1);

        return calendar.getTime();
    }

}
