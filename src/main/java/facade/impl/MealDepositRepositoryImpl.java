package facade.impl;

import facade.MealDepositRepository;
import model.Company;
import model.Employee;
import model.MealDeposit;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MealDepositRepositoryImpl implements MealDepositRepository {


    /**
     * Methode permettant de créditer le compte restaurant
     * @param amount
     * @param company
     * @return meal deposit
     */
    @Override
    public MealDeposit sendMealDeposit(int amount, Company company) {
        // Instanciation de l'objet
        MealDeposit mealDeposit = new MealDeposit();

        // Ajout du montant
        mealDeposit.setAmount(amount);

        // Date d'emission
        mealDeposit.setDateEmission(new Date());

        // Ajout de l'entreprise
        mealDeposit.setCompany(company);

        return mealDeposit;
    }

    /**
     * @param employee
     * @return La somme sur la balance gift deposit
     */
    public int countBalance(Employee employee) throws ParseException {
        int balance = 0;
        if (employee.getMealDepositBalance() != null) {
            for (int i = 0; i < employee.getMealDepositBalance().size(); i++) {
                if (checkDate(employee.getMealDepositBalance().get(i).getDateEmission())) {
                    employee.getMealDepositBalance().remove(i);
                    break;
                }
                balance += employee.getMealDepositBalance().get(i).getAmount();
            }
        }
        return balance;
    }

    /**
     * Methode de vérification de la date d'expiration
     *
     * @param date
     * @return true si la date a expiré
     */
    public boolean checkDate(Date date) throws ParseException {
        Date dateExpiration = new SimpleDateFormat("yyyyMMdd").parse("20230228");
        return date.after(dateExpiration);
    }


}
