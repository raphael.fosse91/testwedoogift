package facade;

import model.Company;
import model.MealDeposit;

public interface MealDepositRepository {

    MealDeposit sendMealDeposit(int amount, Company company);

}
