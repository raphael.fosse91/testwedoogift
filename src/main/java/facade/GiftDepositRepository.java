package facade;

import model.Company;
import model.GiftDeposit;

public interface GiftDepositRepository {

    GiftDeposit sendGiftDeposit(int amount, Company company);

}
