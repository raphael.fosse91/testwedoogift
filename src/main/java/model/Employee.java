package model;

import java.util.List;

/**
 * Objet representant l'employe
 */
public class Employee {

    private String name;
    public List<GiftDeposit> giftDepositBalance;
    public List<MealDeposit> mealDepositBalance;

    public Employee(String name, List<GiftDeposit> giftDepositBalance, List<MealDeposit> mealDepositBalance) {
        this.name = name;
        this.giftDepositBalance = giftDepositBalance;
        this.mealDepositBalance = mealDepositBalance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<GiftDeposit> getGiftDepositBalance() {
        return giftDepositBalance;
    }


    public List<MealDeposit> getMealDepositBalance() {
        return mealDepositBalance;
    }

    public void setMealDepositBalance(List<MealDeposit> mealDepositBalance) {
        this.mealDepositBalance = mealDepositBalance;
    }
}
