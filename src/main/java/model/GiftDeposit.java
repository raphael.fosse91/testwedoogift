package model;

import java.util.Date;

/**
 * Objet representant l'objet carte cadeau
 */
public class GiftDeposit {

    private int amount;
    private Date dateExpiration;
    private Company company;

    public GiftDeposit() {
    }

    public GiftDeposit(int amount, Date dateExpiration) {
        this.amount = amount;
        this.dateExpiration = dateExpiration;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Date getDateExpiration() {
        return dateExpiration;
    }

    public void setDateExpiration(Date dateExpiration) {
        this.dateExpiration = dateExpiration;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
