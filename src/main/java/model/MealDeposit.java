package model;

import java.util.Date;

/**
 * Objet representant l'objet carte restaurant
 */
public class MealDeposit {

    private int amount;
    private Date dateEmission;
    private Company company;

    public MealDeposit() {
    }

    public MealDeposit(int amount, Date dateEmission) {
        this.amount = amount;
        this.dateEmission = dateEmission;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Date getDateEmission() {
        return dateEmission;
    }

    public void setDateEmission(Date dateEmission) {
        this.dateEmission = dateEmission;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
