package facade.impl;

import model.Company;
import model.Employee;
import model.GiftDeposit;
import model.MealDeposit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DepositRepositoryImplTest {

    private GiftDepositRepositoryImpl giftDepositRepository;
    private MealDepositRepositoryImpl mealDepositRepository;

    @BeforeEach
    void init() {
        this.giftDepositRepository = new GiftDepositRepositoryImpl();
        this.mealDepositRepository = new MealDepositRepositoryImpl();
    }


    @Test
    void testCreditCompte() throws ParseException {
        Employee Raphael = new Employee("Raphael", new ArrayList<>(), new ArrayList<>());
        Employee Agathe = new Employee("Agathe", new ArrayList<>(), new ArrayList<>());
        Company Apple = new Company("Apple");
        Company Tesla = new Company("Tesla");

        // Gift deposit
        Raphael.getGiftDepositBalance().add(giftDepositRepository.sendGiftDeposit(100, Apple));
        Raphael.getGiftDepositBalance().add(giftDepositRepository.sendGiftDeposit(150, Tesla));
        Agathe.getGiftDepositBalance().add(giftDepositRepository.sendGiftDeposit(150, Apple));
        Agathe.getGiftDepositBalance().add(giftDepositRepository.sendGiftDeposit(150, Tesla));

        // Meal deposit
        Raphael.getMealDepositBalance().add(mealDepositRepository.sendMealDeposit(100, Apple));
        Raphael.getMealDepositBalance().add(mealDepositRepository.sendMealDeposit(80, Tesla));
        Agathe.getMealDepositBalance().add(mealDepositRepository.sendMealDeposit(15, Apple));
        Agathe.getMealDepositBalance().add(mealDepositRepository.sendMealDeposit(15, Tesla));

        assertNotNull(Agathe.getMealDepositBalance());
        assertNotNull(Raphael.getMealDepositBalance());
        assertNotNull(Agathe.getGiftDepositBalance());
        assertNotNull(Raphael.getGiftDepositBalance());

        assertEquals(250, giftDepositRepository.countBalance(Raphael));
        assertEquals(300, giftDepositRepository.countBalance(Agathe));
        assertEquals(180, mealDepositRepository.countBalance(Raphael));
        assertEquals(30, mealDepositRepository.countBalance(Agathe));
    }


    @Test
    void testGiftDepositExpire() throws ParseException {

        // Initialisation
        Employee Raphael = new Employee("Raphael", new ArrayList<>(), new ArrayList<>());
        Employee Agathe = new Employee("Agathe", new ArrayList<>(), new ArrayList<>());
        Company Apple = new Company("Apple");
        Company Tesla = new Company("Tesla");

        // Gift deposit
        Raphael.getGiftDepositBalance().add(giftDepositRepository.sendGiftDeposit(100, Apple));
        Raphael.getGiftDepositBalance().add(giftDepositRepository.sendGiftDeposit(150, Apple));
        Agathe.getGiftDepositBalance().add(giftDepositRepository.sendGiftDeposit(150, Tesla));
        Agathe.getGiftDepositBalance().add(giftDepositRepository.sendGiftDeposit(150, Tesla));

        // Meal deposit
        Raphael.getMealDepositBalance().add(mealDepositRepository.sendMealDeposit(100, Apple));
        Raphael.getMealDepositBalance().add(mealDepositRepository.sendMealDeposit(80, Apple));
        Agathe.getMealDepositBalance().add(mealDepositRepository.sendMealDeposit(15, Tesla));
        Agathe.getMealDepositBalance().add(mealDepositRepository.sendMealDeposit(15, Tesla));


        // Ajout d'un crédit cadeau expiré
        GiftDeposit giftDepositDateExpiracy = new GiftDeposit();
        giftDepositDateExpiracy.setAmount(150);
        Date dateGift = TestDateUtils.parseDate("2020-02-14");
        giftDepositDateExpiracy.setDateExpiration(dateGift);
        Raphael.getGiftDepositBalance().add(giftDepositDateExpiracy);

        // Ajout d'un crédit cadeau expiré
        MealDeposit mealDeposit = new MealDeposit();
        mealDeposit.setAmount(150);
        Date dateMeal = TestDateUtils.parseDate("2024-02-14");
        mealDeposit.setDateEmission(dateMeal);
        Raphael.getMealDepositBalance().add(mealDeposit);

        assertNotNull(Agathe.getMealDepositBalance());
        assertNotNull(Raphael.getMealDepositBalance());
        assertNotNull(Agathe.getGiftDepositBalance());
        assertNotNull(Raphael.getGiftDepositBalance());

        assertEquals("Apple", Raphael.getMealDepositBalance().get(0).getCompany().getName());
        assertEquals("Tesla", Agathe.getGiftDepositBalance().get(0).getCompany().getName());

        assertEquals(250, giftDepositRepository.countBalance(Raphael));
        assertEquals(300, giftDepositRepository.countBalance(Agathe));

        assertEquals(180, mealDepositRepository.countBalance(Raphael));
        assertEquals(30, mealDepositRepository.countBalance(Agathe));

    }

}
